//
//  main.cpp
//  XO
//
//  Created by Panpech Pothong on 11/9/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//

#include <iostream>
#include<cstdlib>
#include<ctime>
#include <limits>
using namespace std;
void print(int table[3][3]) {
    cout << "---------";
    cout << endl;
    for (int i = 0; i <= 2; i++) {
        for (int j = 0; j <= 2; j++) {
            if (table[i][j] == 0) {
                cout <<"|"<<" "<<"|";
            }
            else if (table[i][j] == 1) {
                cout << "|" << "X" << "|";
            }
            else if (table[i][j] == 2) {
                cout << "|" << "O" << "|";
            }
        }
        cout << endl;
        cout << "---------";
        cout << endl;
    }
}

void your_turn(int table[3][3]) {
    
    int choose;
    int check=0;
    while (check == 0) {
        cout << "Your turn, select: ";
        cin >> choose;
        switch (choose)
        {
            case 1:
                if(table[0][0]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[0][0] = 1;
                check = 1;
                
                break;
            case 2:
                if(table[0][1]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[0][1] = 1;
                check = 1;
                
                break;
            case 3:
                if(table[0][2]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[0][2] = 1;
                check = 1;
                break;
            case 4:
                if(table[1][0]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[1][0] = 1;
                check = 1;
                break;
            case 5:
                if(table[1][1]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[1][1] = 1;
                check = 1;
                break;
            case 6:
                if(table[1][2]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[1][2] = 1;
                check = 1;
                break;
            case 7:
                if(table[2][0]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[2][0] = 1;
                check = 1;
                break;
            case 8:
                if(table[2][1]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[2][1] = 1;
                check = 1;
                break;
            case 9:
                if(table[2][2]==1)
                {
                    cout << "Please Try again " << endl;
                    your_turn(table);
                }
                table[2][2] = 1;
                check = 1;
                break;
            default:
                cout << "You can't do it !" << endl;
                break;
        }
    }
}


bool check(int table[3][3]) {
    
    for (int i = 0; i < 3; i++)
    {
        if ((table[i][0] == table[i][1]&& table[i][1] == table[i][2]&& table[i][0]!=0)|| (table[0][i] == table[1][i] && table[1][i] == table[2][i] && table[0][i] != 0))
        {
            if (table[i][0] == 1|| table[0][i] == 1)
            {
                cout << "Your winner !!!" << endl;
            }
            else if (table[i][0] == 2|| table[0][i] == 2)
            {
                cout << "AI winner !!!" << endl;
            }
            return true;
        }
    }
    if ((table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[1][1] != 0) || (table[2][0] == table[1][1]&& table[1][1] == table[0][2] && table[1][1] != 0))
    {
        if (table[0][0] == 1 || table[0][2] == 1)
        {
            cout << "Your winner !!!" << endl;
        }
        else if (table[0][0] == 2|| table[0][2] == 2)
        {
            cout << "AI winner !!!" << endl;
        }
        return true;
    }
    return false;
}

void table_default(int table[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            table[i][j] = 0;
        }
    }
}

void AI(int table[3][3],int first) {
    
    int check = 0;
    cout << endl;
    cout << "AI turn" << endl;
    if (first != 1&&table[1][1]==0) {
        if (table[0][0] == 1 || table[0][2] == 1 || table[2][0] == 1 || table[2][2] == 1) {
            table[1][1] = 2;
            check = 1;
        }
    }
    if (first == 3 && check == 0) {
        if (table[0][0] == 1 && table[1][1] == 2 && table[2][2] == 1) {
            table[1][0] = 2;
            check = 1;
        }
        if (table[0][2] == 1 && table[1][1] == 2 && table[2][0] == 1) {
            table[1][0] = 2;
            check = 1;
        }
    }
    
    
    
    for (int i = 0; i <= 2 && check == 0; i++) {
        
        if (table[i][0] == 2 && table[i][1] == 2 && table[i][2] == 0 && check == 0) {
            table[i][2] = 2;
            check = 1;
            break;
        }
        else if (table[i][2] == 2 && table[i][1] == 2 && table[i][0] == 0 && check == 0) {
            table[i][0] = 2;
            check = 1;
            break;
        }
        else if (table[i][0] == 2 && table[i][2] == 2 && table[i][1] == 0 && check == 0) {
            table[i][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][i] == 2 && table[2][i] == 2 && table[1][i] == 0 && check == 0) {
            table[1][i] = 2;
            check = 1;
            break;
        }
        else if (table[0][i] == 2 && table[1][i] == 2 && table[2][i] == 0 && check == 0) {
            table[2][i] = 2;
            check = 1;
            break;
        }
        else if (table[2][i] == 2 && table[1][i] == 2 && table[0][i] == 0 && check == 0) {
            table[0][i] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 2 && table[1][1] == 2 && table[2][2] == 0 && check == 0) {
            table[2][2] = 2;
            check = 1;
            break;
        }
        else if (table[0][2] == 2 && table[1][1] == 2 && table[2][0] == 0 && check == 0) {
            table[2][0] = 2;
            check = 1;
            break;
        }
        else if (table[2][2] == 2 && table[1][1] == 2 && table[0][0] == 0 && check == 0) {
            table[0][0] = 2;
            check = 1;
            break;
        }
        else if (table[2][0] == 2 && table[1][1] == 2 && table[0][2] == 0 && check == 0) {
            table[0][2] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 2 && table[2][2] == 2 && table[1][1] == 0 && check == 0) {
            table[1][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][2] == 2 && table[2][0] == 2 && table[1][1] == 0 && check == 0) {
            table[1][1] = 2;
            check = 1;
            break;
        }
    }
    
    for (int i = 0; i <= 2 && check == 0; i++) {
        
        
        if (table[i][0] ==1 && table[i][1] == 1 && table[i][2]==0 && check == 0) {
            table[i][2] = 2;
            check = 1;
            break;
        }
        else if (table[i][2] == 1&& table[i][1] == 1 && table[i][0] == 0 && check == 0) {
            table[i][0] = 2;
            check = 1;
            break;
        }
        else if (table[i][0] == 1 && table[i][2] == 1 && table[i][1] == 0 && check == 0) {
            table[i][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][i] == 1 && table[2][i] == 1 && table[1][i] == 0 && check == 0) {
            table[1][i] = 2;
            check = 1;
            break;
        }
        else if (table[0][i] ==1 && table[1][i] == 1 && table[2][i] == 0 && check == 0) {
            table[2][i] = 2;
            check = 1;
            break;
        }
        else if (table[2][i] ==1 && table[1][i] == 1 && table[0][i] == 0 && check == 0) {
            table[0][i] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 1 && table[1][1] == 1 && table[2][2] == 0 && check == 0) {
            table[2][2] = 2;
            check = 1;
            break;
        }
        else if (table[0][2] == 1 && table[1][1] == 1 && table[2][0] == 0 && check == 0) {
            table[2][0] = 2;
            check = 1;
            break;
        }
        else if (table[2][2] == 1 && table[1][1] == 1 && table[0][0] == 0 && check == 0) {
            table[0][0] = 2;
            check = 1;
            break;
        }
        else if (table[2][0] == 1 && table[1][1] == 1 && table[0][2] == 0 && check == 0) {
            table[0][2] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 1 && table[2][2] == 1 && table[1][1] == 0 && check == 0) {
            table[1][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][2] == 1 && table[2][0] == 1 && table[1][1] == 0 && check == 0) {
            table[1][1] = 2;
            check = 1;
            break;
        }
    }
    for (int i = 0; i <= 2 && check == 0; i++) {
        
        if (table[i][0] == 2 && table[i][1] == 0 && table[i][2] == 0) {
            table[i][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][i] == 2 && table[1][i] == 0 && table[2][i] == 0) {
            table[1][i] = 2;
            check = 1;
            break;
        }
        else if (table[2][i] == 2 && table[1][i] == 0 && table[0][i] == 0) {
            table[1][i] = 2;
            check = 1;
            break;
        }
        else if (table[i][2] == 2 && table[i][1] == 0 && table[i][0] == 0) {
            table[i][1] = 2;
            check = 1;
            break;
        }
        else if (table[i][2] == 2 && table[i][1] == 0 && table[i][0] == 0) {
            table[i][1] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 0 && table[0][1] == 2 && table[0][2] == 0) {
            table[0][0] = 2;
            check = 1;
            break;
        }
        else if (table[0][0] == 0 && table[1][0] == 2 && table[2][0] == 0) {
            table[0][0] = 2;
            check = 1;
            break;
        }
        else if (table[0][2] == 0 && table[1][2] == 2 && table[2][2] == 0) {
            table[0][2] = 2;
            check = 1;
            break;
        }
        else if (table[2][0] == 0 && table[2][1] == 2 && table[2][2] == 0) {
            table[2][0] = 2;
            check = 1;
            break;
        }
    }
    
    if (check == 0) 
    {
        while (check == 0)
        {
            int  botchoose_x = rand() % 3;
            int  botchoose_y = rand() % 3;
            if (table[botchoose_x][botchoose_y] == 0) 
            {
                table[botchoose_x][botchoose_y] = 2;
                check = 1;
            }
        }
    }
}


void rule() {
    
    cout << "======== Table =======" << endl;
    cout << "---------"<<endl;
    char play[3][3] = {'1','2','3','4','5','6','7','8','9'};
    for (int i = 0; i <3; i++) {
        for (int j = 0; j <3; j++) {
            cout << "|" << play[i][j] << "|";
            
        }
        cout << endl;
        cout << "---------";
        cout << endl;
    }
}


int main(int argc, const char * argv[]) {
    int table[3][3] = { { 0,0,0 },{ 0,0,0 },{ 0,0,0 } };
    
    int check_start = 0;
    int playagain;
    
    rule();
    cout << endl;
    
    
    while (playagain != -1) 
    {
        cout << "======= GAME START =======\n\n";
        srand(time(0));
        check_start = rand() % 2;
        
        if (check_start == 0) 
        {
            cout << "You first!!!" << endl;
        }
        else 
        {
            cout << "AI first!!!" << endl;
        }
        
        for (int i = 1; i <= 9; i++) 
        {
            if (check_start % 2 == 0) 
            {
                your_turn(table);
            }
            else if (check_start % 2 == 1) 
            {
                AI(table, check_start);
            }
            cout << endl;
            print(table);
            check_start++;
            
            if(check(table)){
                break;
            }
            if (i == 9 && ! check(table)) 
            {
                cout << "Draw !!!" << endl;
            }
            
        }
        
        playagain = 2;
        while(playagain != -1 && playagain != 1)
        {	
            cout << "You will play again ?(enter 1 = Yes or enter -1 = No) : ";
            cin >> playagain;
            
            if(playagain != -1 && playagain != 1)
            {
                cout << "Please enter again!! \n" << endl;
            }
            cout << endl;
        }
    }
    
    return 0;
}

